﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public void AtualizarQualidade()
        {
            foreach (var item in Itens)
            {
                if (item.Nome == "Dente do Tarrasque") continue;

                if (item.Nome != "Ingressos para o concerto do Turisas")
                    item.PrazoParaVenda -= 1;

                switch (item.Nome)
                {
                    case "Queijo Brie Envelhecido":
                        item.Qualidade += 1;
                        if (item.PrazoParaVenda < 0) item.Qualidade += 1;
                        break;
                    case "Ingressos para o concerto do Turisas":
                        item.Qualidade += 1;
                        if (item.PrazoParaVenda < 6)
                        {
                            item.Qualidade += 2;
                        }
                        else if (item.PrazoParaVenda < 11)
                        {
                            item.Qualidade += 1;
                        } 
                        break;
                    case "Bolo de Mana Conjurado":
                        item.Qualidade -= 2;
                        if (item.PrazoParaVenda < 0) item.Qualidade -= 2;
                        break;
                    default: 
                        item.Qualidade -= 1;
                        if (item.PrazoParaVenda < 0) item.Qualidade -= 1;
                        break;
                }

                if (item.Nome == "Ingressos para o concerto do Turisas")
                {
                    item.PrazoParaVenda -= 1;
                    if (item.PrazoParaVenda < 0) item.Qualidade = 0;
                }

                if (item.Qualidade < 0)
                    item.Qualidade = 0;
                else
                {
                    if (item.Qualidade > 50) item.Qualidade = 50;
                }
            }
        }
    }
}
